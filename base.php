<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" lang="fr">
    <title>Maison - <?php echo $roomName;?> </title>
    <script src="https://meet.jit.si/external_api.js"></script>
    <script>
        var roomName = "<?php echo $roomName; ?>";
    </script>
    <script src="../base.js" defer></script>
    <script src="index.js" defer></script>
    <link rel="stylesheet" type="text/css" href="../base.css">
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
    <div id="meet"></div>
