<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" lang="fr">
	<title>Toilettes</title>
	<!-- <link rel="stylesheet" type="text/css" href=".css"> -->
	<!-- <script src=".js"></script> -->
	<!-- <script src='https://meet.jit.si/external_api.js'></script> -->
	<script>
		var init = function(){
			console.log(":init");
		}
		window.addEventListener("DOMContentLoaded",init);
	</script>
</head>

<body>
	<header>
		<div id="msg">
			<p>
				message sur la jauge des salles
			</p>
		</div>
	</header>
	<main>
		<?php
			$room = "toilettes";
			include "./frame.php?room=".$room;
		?>
	</main>
	<aside>
		<div id="decor"></div>
	</aside>
	<footer>
		<div id="mentions"></div>
	</footer>
</body>
</html>